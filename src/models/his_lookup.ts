import { AxiosInstance } from "axios";

export class HISLookupModel {
  getWaitingList(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = '/services/patient?an=' + an;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }
}